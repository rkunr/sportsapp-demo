﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sportsweb_Trial.Controllers
{
    public class CookieController : Controller
    {
        // GET: Cookie
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult CookieConsent()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SaveCookie()
        {
            string success = "true";
            //let's create and save cookie
            HttpCookie consentCookie = new HttpCookie("RitzauAcceptCookie");
           // consentCookie.Values.Add("Id", Guid.NewGuid().ToString());            
            consentCookie.Values.Add("CreatedDate", DateTime.Now.ToString());
           // consentCookie.Domain = "ritzau.dk";
#if !DEBUG
            consentCookie.Secure = true;
#else
            consentCookie.Secure = false;
#endif

            consentCookie.Expires = DateTime.MaxValue;
            try
            {
                System.Web.HttpContext.Current.Response.Cookies.Add(consentCookie);
            }
            catch(Exception e)
            {
                success = "false";
            }
            return Json(new { cookiename = "RitzauAcceptCookie", createdDate = DateTime.Now , Succeed=success});

        }
    }
}