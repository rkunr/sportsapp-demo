﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Sportsweb_Trial.DAL;
using Sportsweb_Trial.Models;

namespace Sportsweb_Trial.Controllers
{
    [RitzauAcceptCookieCheck]
    public class HomeController : Controller
    {
       
        public ActionResult Index()
        {
            string[] sportForTheApp = {"Fodbold", "Håndbold","Tennis","Ishockey","Basketball"};
            SportsIndexModel model = new SportsIndexModel(sportForTheApp);
           
            
            return View(model);
        }

        public ActionResult SportsDetail()
        {
           SportswebModel model = new SportswebModel();
            return View(model);
        }
        public ActionResult Leagues(int sportsId=1)
        {
            SportsLeagueModel model = new SportsLeagueModel(sportsId);
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult CookieConsent()
        {
            return View();
        }

        public ActionResult ShowClubDetails(int rowId, int hovedturneringId, int holdId=9999)
        {
            ClubDetailsModel model = new ClubDetailsModel(rowId, holdId, hovedturneringId);           
            if(model == null)
            {
                //redirect to an error handling page later on
                return View();
            }            
            else
                return View(model);
        }
    }
}