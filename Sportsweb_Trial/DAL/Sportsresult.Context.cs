﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sportsweb_Trial.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class SportsWebEntities : DbContext
    {
        public SportsWebEntities()
            : base("name=SportsWebEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Hold> Holds { get; set; }
        public virtual DbSet<HovedTurnering> HovedTurnerings { get; set; }
        public virtual DbSet<Idraet> Idraets { get; set; }
        public virtual DbSet<IdraetsNote> IdraetsNotes { get; set; }
        public virtual DbSet<Kampinfo> Kampinfoes { get; set; }
        public virtual DbSet<Land> Lands { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Raekke> Raekkes { get; set; }
        public virtual DbSet<Resultat> Resultats { get; set; }
        public virtual DbSet<Saeson> Saesons { get; set; }
        public virtual DbSet<Stilling> Stillings { get; set; }
        public virtual DbSet<Topscore> Topscores { get; set; }
        public virtual DbSet<Tilskuere> Tilskueres { get; set; }
    }
}
