﻿using Sportsweb_Trial.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sportsweb_Trial.Models
{
    public class SportsLeagueModel
    {
        public List<RaekkeExt> DanskTurnering;
        public List<string> InternationalTurnering;
        public SportsLeagueModel(int gameId)
        {
           
            DanskTurnering = this.GetDanskTurnering(gameId);
        }

        public List<RaekkeExt> GetDanskTurnering(int idraetId)
        {
            SportsWebEntities ents = new SportsWebEntities();
            var result = (from r in ents.Raekkes
                          join i in ents.Idraets on r.Raekke_Idraet_FN equals i.Idraet_ID
                          where i.Idraet_ID == 1 && r.Raekke_Land_FN == 1 && r.Raekke_HovedTurnering_FN == 316
                          select new RaekkeExt {
                              Id = r.Raekke_ID,
                              RaekkeGroupId = r.Raekke_RaekkeGrp_FN == null ? -1 : r.Raekke_RaekkeGrp_FN,
                              Name = r.Raekke_Navn,
                              HovedturneringId=r.Raekke_HovedTurnering_FN
                          }).ToList();
            return result;
        }

        public class RaekkeExt
        {
            public int Id { get; set; }
            public int? RaekkeGroupId { get; set; }
            public string Name { get; set; }
            public int? HovedturneringId { get; set; }
            public int HoldId { get; set; }
        }
    }
}