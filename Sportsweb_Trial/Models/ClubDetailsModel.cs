﻿using Sportsweb_Trial.DAL;
using Sportsweb_Trial.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sportsweb_Trial.Models
{
    public class ClubDetailsModel
    {
        //public List<IGrouping<string,ResultatExt>> AllClubDetails;
        public List<ClubDetails> AllClubDetails;
        public string HoldName;
        public string CustomStylling;
        public  ClubDetailsModel(int raekkeId, int holdId, int hovedturneringId, string firmanavn="ritzau.dk")
        {
            AllClubDetails = this.GetClubResults(raekkeId, holdId, hovedturneringId);
            HoldName = this.GetHoldName(holdId);
            CustomStylling = this.GetCustomStylling(firmanavn);
        }

        public string GetHoldName(int holdid)
        {
            SportsWebEntities ents = new SportsWebEntities();
            return ents.Holds.Where(c=>c.Hold_ID==holdid).Select(c=>c.Hold_Navn).FirstOrDefault();
        }
        public string GetCustomStylling(string firmaNavn)
        {
            string ritzauUrl = "https://www.ritzau.dk/ritzaucontent/htmlHelper/css/htmlhelper.css";
            if (firmaNavn == "ritzau.dk")
                return ritzauUrl;
            return ritzauUrl;
        }

        //public List<IGrouping<string,ResultatExt>> GetClubResults(int raekkeId, int holdId, int hovedturneringId)
        public List<ClubDetails> GetClubResults(int raekkeId, int holdId, int hovedturneringId)
        {
            SportsWebEntities ents = new SportsWebEntities();
            //List<IGrouping<string, ResultatExt>> result;
            List<ClubDetails> result;
            int seasonId = ents.Raekkes.Where(r => r.Raekke_ID == raekkeId).Select(r => r.Raekke_Saeson_FN).FirstOrDefault();
            int? hovedTurneringId = ents.Raekkes.Where(r => r.Raekke_ID == raekkeId).Select(r => r.Raekke_HovedTurnering_FN).FirstOrDefault();

            if (seasonId == 0 && hovedTurneringId == null)
            {
                result = null; //if we did not find a season for the given raekkeId, we will send error message from controller
            }
            else
            {
                #region oldcode
                //result = (from r in ents.Resultats.OrderByDescending(r => r.Resultat_kampDato)
                //                      where r.Resultat_Hovedturnering_FN == hovedTurneringId &&
                //                            r.Resultat_Saeson_FN == seasonId && r.Resultat_Resultat != null &&
                //                            (r.Resultat_HjemmeHold_FN == holdId || r.Resultat_Udehold_FN == holdId)
                //                      group new ResultatExt
                //                      {
                //                          LeagueType = r.Resultat_RaekkeNavn
                //                          ,
                //                          GameDate = r.Resultat_kampDato
                //                          ,
                //                          TeamHosting = r.Resultat_HjemmeHold
                //                          ,
                //                          FinalResult = r.Resultat_Resultat
                //                          ,
                //                          ScoreFirstPeriod = r.Resultat_1Periode
                //                          ,
                //                          TeamVisiting = r.Resultat_UdeHold
                //                      } by r.Resultat_RaekkeNavn into game
                //                      select game).ToList();
                #endregion
                result = ents.Resultats
                    .Where(r => r.Resultat_Saeson_FN == seasonId && r.Resultat_Hovedturnering_FN == hovedTurneringId && (r.Resultat_Udehold_FN == holdId || r.Resultat_HjemmeHold_FN == holdId) && r.Resultat_Resultat != null)
                    .GroupBy(r => new { r.Resultat_RaekkeNavn, r.Resultat_SaesonNavn }, (key, group) => new ClubDetails
                    {
                        TurneringNavn = key.Resultat_RaekkeNavn,
                        SaesonNavn = key.Resultat_SaesonNavn,
                        AlleResultat = group.ToList()
                    }).ToList();
            }
            return result;
        }

        public class ClubDetails
        {
            public string TurneringNavn { get; set; }
            public string SaesonNavn { get; set; }
            public List<Resultat> AlleResultat { get; set; }
        }

        public class FirmaStylling
        {
            public string CustomStylling { get; set; }
        }
    }
}