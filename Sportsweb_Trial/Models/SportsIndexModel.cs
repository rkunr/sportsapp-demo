﻿using Sportsweb_Trial.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sportsweb_Trial.Models
{
    public class SportsIndexModel
    {
        public List<IdreatExt> SportsForTheApp;
        public SportsIndexModel(string[] sportsCovering)
        {
            SportsForTheApp = this.GetSportsForTheWeb(sportsCovering);
        }
       
        public List<IdreatExt> GetSportsForTheWeb(string[] sports)
        {
            SportsWebEntities ents = new SportsWebEntities();
            var result = ents.Idraets.Where(c => sports.Contains(c.Idraet_Navn)).Select(c => new IdreatExt
            {
                IdraetId = c.Idraet_ID,
                IdraetNavn = c.Idraet_Navn,
                IdreatLogoURL = c.Idraet_FlagURL
            }).OrderBy(o=>o.IdraetNavn).ToList();
            return result;
        }

        public class IdreatExt
        {
            public int IdraetId { get; set; }
            public string IdraetNavn { get; set; }
            public string IdreatLogoURL { get; set; }
        }

    }
}