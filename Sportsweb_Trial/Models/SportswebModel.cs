﻿using Sportsweb_Trial.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Sportsweb_Trial.Models
{
    public class SportswebModel
    {
        public List<SportsViewModel> SportsStilling;
        public SportswebModel()
        {
            SportsStilling = this.GetSportsStilling();
        }

        public List<SportsViewModel> GetSportsStilling()
        {
            SportsWebEntities ents = new SportsWebEntities();
            var result = (from s in ents.Stillings
                          join r in ents.Raekkes on s.Stilling_Raekke_FN equals r.Raekke_ID
                          join h in ents.HovedTurnerings on r.Raekke_HovedTurnering_FN equals h.HovedTurnering_ID
                          join sa in ents.Saesons on h.HovedTurnering_Saeson_FN equals sa.Saeson_ID
                          where sa.Saeson_Aktiv == true && r.Raekke_RaekkeGrp_FN == 311 /*&& s.Stilling_Raekke_FN == 142171*/
                          select new SportsViewModel
                          {
                              RaekkeId=s.Stilling_Raekke_FN,
                              HoldId=s.Stilling_Hold_FN,
                              HovedTurneringId=r.Raekke_HovedTurnering_FN,
                              Position = s.Stilling_Position,
                              ForbundNavn = s.Stilling_RaekkeNavn,
                              ClubName = s.Stilling_Hold,
                              PlayedTotal = (s.Stilling_HjemmeSpillet + s.Stilling_UdeSpillet),
                              WonTotal = (s.Stilling_HjemmeVundet + s.Stilling_UdeVundet),
                              LostTotal = (s.Stilling_HjemmeTabt + s.Stilling_UdeTabt),
                              DrawTotal = (s.Stilling_HjemmeUafgjort + s.Stilling_UdeUafgjort),
                              GoalFor = (s.Stilling_HjemmeMaalFor + s.Stilling_UdeMaalFor),
                              GoalAgainst = (s.Stilling_HjemmeMaalImod + s.Stilling_UdeMaalImod),
                              PointTotal = (s.Stilling_HjemmePoint + s.Stilling_UdePoint)
                          }).AsNoTracking().ToList();
            return result;
        }
        public class SportsViewModel
        {
            public string ForbundNavn { get; set; } //table=Resultat_ForbundNavn
            public string ClubName { get; set; } //table=Stilling //field=Stilling_Hold
            public int? PlayedTotal { get; set; }
            public int? WonTotal { get; set; }
            public int? LostTotal { get; set; }
            public int? DrawTotal { get; set; }
            public int? GoalFor { get; set; }
            public int? GoalAgainst { get; set; }
            public int? HoldId { get; set; }
            public int? RaekkeId { get; set; }
            public int? HovedTurneringId { get; set; }

            public string GoalForAgainst  {
                set
                {
                    value = GoalFor + " - " + GoalAgainst;
                }
                get
                {
                    return GoalFor + " - " + GoalAgainst;
                }
            }
            public int? PointTotal { get; set; }
            public int? Position { get; set; }
        }
    }
}