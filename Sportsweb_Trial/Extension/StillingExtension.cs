﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sportsweb_Trial.Extension
{
    public class StillingExtension
    {
        public string ForbundNavn { get; set; } //table=Resultat_ForbundNavn
        public string ClubName { get; set; } //table=Stilling //field=Stilling_Hold
        public int? PlayedTotal { get; set; }
        public int? WonTotal { get; set; }
        public int? LostTotal { get; set; }
        public int? DrawTotal { get; set; }
        public int? GoalFor { get; set; }
        public int? GoalAgainst { get; set; }
        public int? HoldId { get; set; }
        public int? RaekkeId { get; set; }

        public string GoalForAgainst
        {
            set
            {
                value = GoalFor + " - " + GoalAgainst;
            }
            get
            {
                return GoalFor + " - " + GoalAgainst;
            }
        }
        public int? PointTotal { get; set; }
        public int? Position { get; set; }
    }
}