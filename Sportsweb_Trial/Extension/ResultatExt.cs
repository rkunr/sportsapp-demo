﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sportsweb_Trial.Extension
{
    public class ResultatExt
    {
        public int KampId { get; set; }
        public string LeagueType { get; set; } //Resultat_RaekkeNavn
        public string SportsName { get; set; } //Resultat_IdraetNavn
        public string SeasonName { get; set; } //Resultat_SaesonNavn
        public string Organization { get; set; }//Resultat_ForbundNavn
        public string Country { get; set; } //Resultat_LandNavn
        public string FinalResult { get; set; } //Resultat_Resultat
        public int ScoreHome { get; set; } //Resultat_HjemmeScore
        public int ScoreAway { get; set; } //Resultat_UdeScore
        public string ScoreFirstPeriod { get; set; } //Resultat_1Periode
        public string ScoreSecondPeriod { get; set; } //Resultat_2Periode
        public string ScoreThirdPeriod { get; set; } //Resultat_3Periode
        public string ScoreFourthPeriod { get; set; } //Resultat_4Periode
        public string ResultRemarks { get; set; } //Resultat_ResultInfo gives info about overtime
        public DateTime? GameDate { get; set; } //Resultat_KampDato
        public int TeamHostingId { get; set; } //Resultat_HjemmeHOld_FN Id of the team who is hosting 
        public string TeamHosting { get; set; } //Resultat_Hjemmehold
        public int TeamVisitingId { get; set; } //Resultat_Udehold_FN Id of the team who is hosting 
        public string TeamVisiting { get; set; } //Resultat_Udehold

        public string TournamentType { get; set; } //REsultat_RaekkeNavn


    }
}