﻿using Sportsweb_Trial.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sportsweb_Trial
{
    public class RitzauAcceptCookieCheck : ActionFilterAttribute
    {
        public string originalRequest { get; private set; }
        private bool _flag = true;
        //public RouteValueDictionary CookiesConsent { get;  set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

           // base.OnActionExecuting(filterContext);
            var cookies = filterContext.HttpContext.Request.Cookies["RitzauAcceptCookie"];
            var values = filterContext.RouteData.Values.Values;
            originalRequest =  filterContext.HttpContext.Request.Url.AbsoluteUri;
            RouteValueDictionary requestOrigin = new RouteValueDictionary { { "url", originalRequest } };

            if (cookies == null && !values.Contains("CookieConsent"))
            {
                //code to make a cookie



                ////redirect with new route value as we will not be sure if there is always HomeController
                //filterContext.Result = new RedirectToRouteResult("CookiesConsent",
                //        new System.Web.Routing.RouteValueDictionary(
                //            new
                //            {
                //                action = "CookieConsent",
                //                controller = "Cookie"
                //            }));
                UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);
                filterContext.Result = new RedirectResult(urlHelper.Action("CookieConsent", "Cookie"));
                //filterContext.Result = new RedirectResult(urlHelper.Action("CookieConsent","Cookie",null,"https","www.ritzau.dk/kontakt"));

            /********************   below code is for sending reqeust to another domain  ***********************/

                //UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);
                //filterContext.Result = new RedirectResult(urlHelper.Action("CookieConsent", "Cookie", requestOrigin, "https", "www.ritzau.dk/kontakt"));

                //redirect to a default route value with a view
                /*here we re-direct to https://www.ritzau.dk/kontakt/Cookie/CookieConsent/             *******/


            }
            else if(cookies != null && _flag)
            {
                string controllerName =  filterContext.RouteData.Values["controller"].ToString();
                string actionName = filterContext.RouteData.Values["action"].ToString();
               // string idName = filterContext.RouteData.Values["id"].ToString();
                UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);
                filterContext.Result = new RedirectResult(urlHelper.ActionAbsolute(actionName, controllerName));
                //filterContext.Result = new RedirectResult(urlHelper.Action(actionName,controllerName));
                _flag = false;
            }
        }
    }
}