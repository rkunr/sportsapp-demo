﻿
var HtmlHelpers = {

    PopMessage: function (headline, text) {
        $(".infoHeadline").text(headline);
        $(".infoText").html(text);
        $(".infoArea").fadeIn(200);
    },
    PopMessageClose: function () {
        $(".infoArea").fadeOut(200);
    },
    ChckmarkfieldonClick: function (item, eventCall) {

        var isActive = HtmlHelpers.chmarkToggle(item);
        if (eventCall != undefined) {
            executeFunctionByName(eventCall, window, item, isActive);
        }
    },
    /** @description set the checkmark state to checked
     * @param {object} item are the checkmark div to be set to checked
     * @returns {bool} the state of the check mark box
     */
    setChckMark: function (item) {
        var $item = $(item);
        var checked = "", unchecked = "";
        if ($item.attr("blue") == "true") {
            checked = "cmfCheckedBlue";
            unchecked = "cmfUnCheckedBlue";
        } else {
            checked = "cmfChecked";
            unchecked = "cmfUnChecked";
        }
        var isActive = !$item.hasClass(checked);
        if (isActive) {
            $(item).removeClass(unchecked);
            $(item).addClass(checked);
        }
        return $item.hasClass(checked);
    },
    /** @description set the checkmark state to unchecked
     * @param {object} item are the checkmark div to be set to unchecked
     * @returns {bool} the state of the check mark box
     */
    removeChckMar: function (item) {
        var $item = $(item);
        var checked = "", unchecked = "";
        if ($item.attr("blue") == "true") {
            checked = "cmfCheckedBlue";
            unchecked = "cmfUnCheckedBlue";
        } else {
            checked = "cmfChecked";
            unchecked = "cmfUnChecked";
        }
        var isActive = $item.hasClass(checked);
        if (isActive) {
            $(item).removeClass(checked);
            $(item).addClass(unchecked);
        }
        return $item.hasClass(unchecked);
    },
    /** @description get the state of the checkmarkbox
     *  @param {object} item are a checkmark div block object
     *  @returns {bool} true if the box is cheked otherwise false
     */
    getCheckMarkState: function (item) {
        var $item = $(item);
        var checked = "", unchecked = "";
        if ($item.attr("blue") == "true") {
            checked = "cmfCheckedBlue";
            unchecked = "cmfUnCheckedBlue";
        } else {
            checked = "cmfChecked";
            unchecked = "cmfUnChecked";
        }
        var isActive = $item.hasClass(checked);
        return isActive;
    },
    ChckmarkMultifieldonChildClick: function (item, eventCall) {
        var isActive = HtmlHelpers.chmarkToggle(item);
        var $masterBox = $(item).parent().siblings(".masterCheckMark").children(".masterCheckMarkText");
        if (isActive) {
            var $unChecked = $(item).siblings(".cmfUnChecked");
            if ($unChecked.length > 0) {
                $masterBox.removeClass("cmfUnCheckedBlue")
                $masterBox.addClass("cmfCheckedHalfBlue");
            } else {
                $masterBox.removeClass("cmfUnCheckedBlue")
                $masterBox.removeClass("cmfCheckedHalfBlue");
                $masterBox.addClass("cmfCheckedBlue");
            }
        } else {
            var $Checked = $(item).siblings(".cmfChecked");
            if ($Checked.length > 0) {
                $masterBox.removeClass("cmfUnCheckedBlue")
                $masterBox.addClass("cmfCheckedHalfBlue");
            } else {
                $masterBox.removeClass("cmfCheckedBlue")
                $masterBox.removeClass("cmfCheckedHalfBlue");
                $masterBox.addClass("cmfUnCheckedBlue");
            }
        }
        if (eventCall != undefined) {
            executeFunctionByName(eventCall, window, item, isActive);
        }
    },
    updateParentCheckMark: function (item, state) {
        var $masterChb = $(item).parent().siblings(".masterCheckMark");
        if ($masterChb.length > 0) {
            var $masterBox = $masterChb.children(".masterCheckMarkText");
            if (state) {
                var $unChecked = $(item).siblings(".cmfUnChecked");
                if ($unChecked.length > 0) {
                    $masterBox.removeClass("cmfUnCheckedBlue")
                    $masterBox.addClass("cmfCheckedHalfBlue");
                } else {
                    $masterBox.removeClass("cmfUnCheckedBlue")
                    $masterBox.removeClass("cmfCheckedHalfBlue");
                    $masterBox.addClass("cmfCheckedBlue");
                }
            } else {
                var $Checked = $(item).siblings(".cmfChecked");
                if ($Checked.length > 0) {
                    $masterBox.removeClass("cmfUnCheckedBlue")
                    $masterBox.addClass("cmfCheckedHalfBlue");
                } else {
                    $masterBox.removeClass("cmfCheckedBlue")
                    $masterBox.removeClass("cmfCheckedHalfBlue");
                    $masterBox.addClass("cmfUnCheckedBlue");
                }
            }
        }
    },
    ChckmarkMultifieldonClick: function (item, eventCall, childboxEventCall) {
        var $item = $(item);
        var childCol = $item.parent().parent().attr("childBlue");
        var checkCss = childCol == "true" ? "cmfCheckedBlue" : "cmfChecked";
        var uncheckCss = childCol == "true" ? "cmfUnCheckedBlue" : "cmfUnChecked";
        var $itemDetail = $item.parent().siblings(".masterCheckDetail");
        var isActive = $item.hasClass("cmfUnCheckedBlue") || $item.hasClass("cmfCheckedHalfBlue");
        if (isActive) {
            $itemDetail.children("." + uncheckCss).each(function () {
                $(this).removeClass(uncheckCss);
                $(this).addClass(checkCss);
                if (childboxEventCall != undefined) {
                    executeFunctionByName(childboxEventCall, window, this, isActive);
                }
            });
            $item.removeClass("cmfCheckedHalfBlue");
            $item.removeClass("cmfUnCheckedBlue");
            $item.addClass("cmfCheckedBlue");
        } else {
            $itemDetail.children("." + checkCss).each(function () {
                $(this).removeClass(checkCss);
                $(this).addClass(uncheckCss);
                if (childboxEventCall != undefined) {
                    executeFunctionByName(childboxEventCall, window, this, isActive);
                }
            });
            $item.removeClass("cmfCheckedBlue");
            $item.addClass("cmfUnCheckedBlue");
        }
        if (eventCall != undefined) {
            executeFunctionByName(eventCall, window, item, isActive);
        }
        var $childParent = $item.next("masterCheckDetail");
    },
    /**
     * @description For changing the curent visual state of a checkmark
     * @param {string} attributName is the name of the attribut to search for
     * @param {array} attributeVal array of items that are present in attributNames value
     */
    updateAllChckMark: function (attributeName, attributeVal) {
        $(".checkmarkfield").each(function (index, elem) {
            var attStrVal = $(elem).attr(attributeName);
            if (attStrVal != undefined) {
                var attValArray = HtmlHelpers.splitToInt(attStrVal);
                if (attValArray != undefined && attValArray.length > 0) {
                    var intersec = HtmlHelpers.intersect(attValArray, attributeVal);
                    var state = false;
                    if (intersec.length > 0) {
                        state = HtmlHelpers.setChckMark(elem);
                    } else {
                        state = !HtmlHelpers.removeChckMar(elem);
                    }
                    HtmlHelpers.updateParentCheckMark(elem, state);
                }
            }
        });
    },
    /** @description get the state of all checkmark which has attribute name where the value contain attributeVal
    * @param {string} attributeName are the name of the attribute to search for
    * @param {int[]} attributeVal an array of intergers that determines if the checkbox are part of the state check
    * @returns {bool} if the all check marks are true it return true else false
    */
    getChckMarkStateFor:function(attributeName, attributeVal) {
        var isChecked = true;
        $(".checkmarkfield").each(function (index, elem) {
            var attStrVal = $(elem).attr(attributeName);
            if (attStrVal != undefined) {
                var attValArray = HtmlHelpers.splitToInt(attStrVal);
                if (attValArray != undefined && attValArray.length > 0) {
                    var intersec = HtmlHelpers.intersect(attValArray, attributeVal);
                    var state = false;
                    if (intersec.length > 0) {
                        isChecked &= HtmlHelpers.getCheckMarkState(elem);
                    }
                }
            }
        });
        return isChecked;
    },
    /** @description set the state of all checkmark, which has attribute name where the value contain attributeVal, to checked executing onclick
    * @param {string} attributeName are the name of the attribute to search for
    * @param {int[]} attributeVal an array of intergers that determines if the checkbox are part of the state check
    * @returns {bool} if the all check marks are true it return true else false
    */
    setChckMarkStateFor: function (attributeName, attributeVal) {
        $(".checkmarkfield").each(function (index, elem) {
            var attStrVal = $(elem).attr(attributeName);
            if (attStrVal != undefined) {
                var attValArray = HtmlHelpers.splitToInt(attStrVal);
                if (attValArray != undefined && attValArray.length > 0) {
                    var intersec = HtmlHelpers.intersect(attValArray, attributeVal);
                    var state = false;
                    if (intersec.length > 0 && !HtmlHelpers.getCheckMarkState(elem)) {
                        $(elem).click();
                    }
                }
            }
        });
    },
    /** @description set the state of all checkmark, which has attribute name where the value contain attributeVal, to unchecked executing onclick
    * @param {string} attributeName are the name of the attribute to search for
    * @param {int[]} attributeVal an array of intergers that determines if the checkbox are part of the state check
    * @returns {bool} if the all check marks are true it return true else false
    */
    removeChckMarkStateFor: function (attributeName, attributeVal) {
        $(".checkmarkfield").each(function (index, elem) {
            var attStrVal = $(elem).attr(attributeName);
            if (attStrVal != undefined) {
                var attValArray = HtmlHelpers.splitToInt(attStrVal);
                if (attValArray != undefined && attValArray.length > 0) {
                    var intersec = HtmlHelpers.intersect(attValArray, attributeVal);
                    var state = false;
                    if (intersec.length > 0 && HtmlHelpers.getCheckMarkState(elem)) {
                        $(elem).click();
                    }
                }
            }
        });
    },
    /**
     * @description For toggling chekmarkboxes on and off
     * @param {this} item of a chekmarkbox that need to be toggled
    */
    chmarkToggle: function (item) {
        var $item = $(item);
        var checked = "", unchecked = "";
        if ($item.attr("blue") == "true") {
            checked = "cmfCheckedBlue";
            unchecked = "cmfUnCheckedBlue";
        } else {
            checked = "cmfChecked";
            unchecked = "cmfUnChecked";
        }
        var isActive = !$item.hasClass(checked);
        if (isActive) {
            $(item).removeClass(unchecked);
            $(item).addClass(checked);
        }
        else {
            $(item).removeClass(checked);
            $(item).addClass(unchecked);
        }
        return isActive;
    },
    hideMultiCheckBox: function (item) {
        var $item = $(item);
        var $details = $item.parent().next(".masterCheckDetail");
        if ($item.hasClass("togDown")) {
            $item.removeClass("togDown");
            $item.addClass("togUp");
        } else {
            $item.removeClass("togUp");
            $item.addClass("togDown");
        }
        $details.slideToggle(150);
    },
    SearchBoxClear: function () {
        $("#searchbox").val("");
        HtmlHelpers.searchClearFocus();
    },
    searchOnFocus: function () {
        var $searchBut = $("#searchbut");
        var $searchbox = $("#searchbox");
        if ($searchBut.hasClass("searchUnsel")) {
            $searchbox.removeClass("searchUnsel");
            $searchBut.removeClass("searchUnsel");
            $searchbox.addClass("searchSel");
            $searchBut.addClass("searchSel");
        }
    },
    searchClearFocus: function () {
        var $searchBut = $("#searchbut");
        var $searchbox = $("#searchbox");
        if ($searchBut.hasClass("searchSel")) {
            $searchbox.removeClass("searchSel");
            $searchBut.removeClass("searchSel");
            $searchbox.addClass("searchUnsel");
            $searchBut.addClass("searchUnsel");
        }
    },
    /**
    * @description Show an info message
    * @param {string} message the message in the inforbox
    * @param {string} titel the titel on the infoboxs
    */
    infoMessageShow: function infoBox(message, title) {
        $('#heading').html(title);
        $('#text').html(message);
        $('#infoModal').modal('show');

    },
    /** @description takes to array of ints and return an array with the intersection of the 2 sets
     * @param {array} array1 an array of ints
     * @param {array} array2 an array of ints
     */
    intersect: function (array1, array2) {
        var returnArr = [];
        array1.forEach(function (element, index) {
            if (array2.indexOf(element) >= 0)
                returnArr.push(element);
        });
        return returnArr;
    },
    /**
     * @description splits a string to an array of ints
     * @param {string} val a string with comma seperated integers
     */
    splitToInt: function(val) {
        var arr = val.split(",");
        var intarr = arr.map(function (value) { return parseInt(value) });
        return intarr;
    }

}

function executeFunctionByName(functionName, context /*, args */) {
    var args = [].slice.call(arguments).splice(2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
}

/*jQuery UI autocomplete*/

$(document).ready(function () {
	var freeText = "";  //Assuming a user might type own string to search something i.e., a string not from the autocomplete suggestion list
	var customId;     //If it is a free text, we assume Id=0
	var source = function (request, response) {
		$.ajax({
			url: urlToController,
			type: 'POST',
			dataType: 'JSON',
			data: { prefix: request.term },
			success: function (data) {

				response($.map(data, function (item) {
					return {
						label: item.autoCompleteKey,
						value: item.Id
					}

				}));
			}
		});
	};


	$('#searchbox').autocomplete({
		source: source,
		minLength: minCharNeededForSearch,
		autoFocus: true,
		response: function (event, ui) {
			if (ui.content.length == 0) {    //if there is no suggestion i.e., a user has input a string which has no suggestion in the list, we assument it as a free text search and set Id to '0'
				freeText = $(this).val();
				customId = '0';
			}
		},
		/*This will prevent displaying 'value (i.e. Id)' while using arrow keys instead of 'label'*/
		focus: function (event, ui) {
			event.preventDefault();
		},

		/*this event is triggered if a user selects a value from the list*/
		select: function (event, ui) {
			event.preventDefault();
			$("#searchbox").val(ui.item.label);     //this will set the first value from the suggestion list to the 'search' input/text field, if a user presses enter without selecting a value from the suggestion list
			freeText = ui.item.label;
			customId = ui.item.value;
            doSearch(customId, freeText);
            $('#searchbut').focus();
            HtmlHelpers.searchClearFocus();
		},

		/*using change event in case a user modify a value from the suggestion list(adverntantly or inadvertantly) and writes his/her own string, we will makle Id as a 0 */
		change: function (event, ui) {
			customId = ui.item ? ui.item.value : 0;
			}
		});

	/*Either a user presses enter key or click the search button*/
    $('#searchbox').keypress(function (event) {
        if (event.which == 13) {
            freeText = $(this).val();
            doSearch(customId, freeText);
            $('#searchbut').focus();
            HtmlHelpers.searchClearFocus();
        }
    });

    
	$('.searchbutton').click(function () {
        freeText = $('#searchbox').val();
        doSearch(customId, freeText);
        $('#searchbut').focus();
        HtmlHelpers.searchClearFocus();
	});

}); /*document.ready close*/

