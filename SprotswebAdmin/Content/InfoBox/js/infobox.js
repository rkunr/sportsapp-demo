﻿

function PopMessage(headline, text) {
    $(".infoHeadline").text(headline);
    $(".infoText").html(text);
    $(".infoArea").fadeIn(200);
}
function PopMessageClose() {
    $(".infoArea").fadeOut(200);
}
