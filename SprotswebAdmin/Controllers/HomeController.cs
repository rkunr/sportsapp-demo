﻿using SportsDAL;
using SprotswebAdmin.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SprotswebAdmin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
                return View();
        }
            
           
     
        public PartialViewResult CreateEditFirmaMedie()
        {
            FirmaMedieFormModel model = new FirmaMedieFormModel();
            return PartialView("_AddFirmaOgMedier", model);
        }

        [HttpGet]
        public JsonResult GetAllFirma()
        {
            SportWebAdminEntities ents = new SportWebAdminEntities();
            var result = ents.FirmaProfils.Select(c=> new
            {
                Id=c.Id,
                Firma=c.FirmaNavn
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}