﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SprotswebAdmin.Controllers
{
    public class EUCookiesController : Controller
    {
       // public string CookieConsent { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cookie = filterContext.HttpContext.Request.Cookies["RitzauAcceptCookie"];
            if (cookie == null)
            {
                cookie = new HttpCookie("RitzauAcceptCookie");
                filterContext.HttpContext.Request.Cookies.Add(cookie);
               // Redirect("https://www.ritzau.dk/kontakt/");
                // RedirectToAction("GiveConsent", "GDRPController");

                //filterContext.Result = new RedirectToRouteResult(
                //        new System.Web.Routing.RouteValueDictionary
                //        {{ "controller","GDRPController"},{"action","GiveConsent" } });
                //Redirect("GDRPController/GiveConsent");
                //RedirectToRoute("CookiesConsent");
            }
            else
            {
                RedirectToRoute("Default");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}