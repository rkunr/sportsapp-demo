﻿$(document).ready(function () {
	$('.flexdatalist').flexdatalist({
		minLength: 1
	});
	

	$('#add-medie').on('click', function (event) {
		sportsAdmin.addInputField(event);
		feather.replace();
	});
	/*changed method is invoked during typing and when a user wrrites his own*/
	$('input.flexdatalist').on('change:flexdatalist', function (event, set, options) {
		//console.log(set.value);
		console.log(set.text);
		sportsAdmin.saveInputValue(set);
	});

	/*when user selects a value from the autocomplete list*/
	$('input.flexdatalist').on('select:flexdatalist', function (event, selected, options) {
		//console.log(set.value);
		console.log("Label: " + selected.label);
		console.log("FirmaId: " + selected.value);
		sportsAdmin.saveInputValue(selected);
	});

	$('#remove-medie-input').on('click', function (event) {
		sportsAdmin.removeInputField(event);
	});
});

var sportsAdmin = {
	addInputField: function (event) {
		var row = event.currentTarget.parentElement.children[0];	
		var countnumber = $(row).attr('medie-count');
		var $addAnothermedieForm = '<div class="form-group row form-group-addmedie-1"><label for="medieNavn" class="col-sm-2 col-form-label"></label><div class="col-sm-9 col-medie"><input medie-count="1" type="text" class="form-control d-inline-block input-medie-navn-1" id="inputMedieNavn" placeholder="Medie navn" /><i data-feather="minus" id="remove-medie-input" class="hideMedieInputField d-inline-block" onclick="sportsAdmin.removeInputField(this);"></i></div></div >';
		$('div.form-group-addmedie').after($addAnothermedieForm);
		
	},
	removeInputField: function (event) {
		var rowToRemove=event.parentElement.parentElement;		
		$(rowToRemove).remove();
	},
	saveInputValue: function (values) {
		console.log('values from inside function ' + values);
	}
};