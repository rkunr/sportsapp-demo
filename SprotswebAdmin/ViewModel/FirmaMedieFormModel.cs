﻿using SportsDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SprotswebAdmin.ViewModel
{
    public class FirmaMedieFormModel
    {
        public List<Firma> AllFirma;
        public FirmaMedieFormModel()
        {
            AllFirma = this.GetFirmaAndCorrespondingMedier();
        }
        public  List<Firma> GetFirmaAndCorrespondingMedier()
        {
            SportWebAdminEntities ents = new SportWebAdminEntities();
            var result = ents.FirmaProfils
                        .Select(c => new Firma
                        {
                            Id = c.Id,
                            FirmaNavn = c.FirmaNavn
                        }).ToList();
            return result;

        }


        public class Firma
        {
            public int Id { get; set; }
            public string FirmaNavn { get; set; }
        }
        public class Medie
        {
            public Guid MedieId { get; set; }

            public string MedieNavn { get; set; }
        }
    
        
        public class OnlyFirma
        {
            public List<Firma> Firma { get; set; }
        }
    }
}